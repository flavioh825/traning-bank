import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private fb: FormBuilder) { }

  formLogin;

  ngOnInit() {
    this.formLogin = this.fb.group({
      cpf: ['']
    });
  }

}
